from os import name
from django.urls import path
from .views import home_view , product_detail

app_name = 'shop'

urlpatterns = [
    path('', home_view, name='home'),
    path('product/<slug>/', product_detail, name='product'),

]
