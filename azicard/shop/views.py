from django.db import models
from django.shortcuts import render, get_object_or_404
from .models import Product
from cart.cart import Cart
from cart.forms import CartAddProductForm
from django.conf import settings


def home_view(request):
    products = Product.objects.filter(is_active=True)

    search = request.GET.get('search') or ''
    if search:
        products = Product.objects.filter(
            slug__icontains=search, is_active=True)

    return render(request, 'shop/index.html', {'products': products})


def product_detail(request, slug):
    product = get_object_or_404(Product, slug=slug, is_active=True)
    cart_product_form = CartAddProductForm()

    return render(request, 'shop/product/detail.html', {'product': product,
                                                        'cart_product_form': cart_product_form})
