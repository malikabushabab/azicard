from django.contrib import admin
from .models import Product, Category


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    # sets values fro  how the admin site lists your product
    list_display = ['name', 'price', 'old_price', 'created_at', 'updated_at',]
    list_display_links = ['name',]
    ordering = ['-created_at']

    search_fields = ['name', 'description', 'meta_keywords', 'meta_description'] 
    exclude = ('created_at', 'updated_at',)

    # sets up slug to be generated from product name
    prepopulated_fields = {'slug': ['name',]}


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    #sets up values for how admin site lists categories
    list_display = ('name', 'created_at', 'updated_at',) 
    search_fields = ['name', 'description', 'meta_keywords', 'meta_description']
    exclude = ('created_at', 'updated_at',)
    ordering = ['-created_at']
    # sets up slug to be generated from category name
    prepopulated_fields = {'slug' : ('name',)} 